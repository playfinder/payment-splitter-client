<?php

namespace Playfinder\PaymentSplitter;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\Utils;
use GuzzleHttp\Exception\ClientException;
use Psr\Cache\CacheItemPoolInterface;
use Ramsey\Uuid\Uuid;

class Client
{
    private HttpClient $client;
    private CacheItemPoolInterface $cache;
    private string $uri;
    private string $email;
    private string $hash;
    private const VALID_RESOURCES = [
        'attendees',
        'payment-groups',
        'split-payments',
    ];

    public function __construct(HttpClient $client, CacheItemPoolInterface $cache, string $uri, string $email, string $hash)
    {
        $this->client = $client;
        $this->cache = $cache;
        $this->email = $email;
        $this->hash = $hash;
        $this->uri = $uri;
    }

    public function postPaymentIntent(string $paymentIntent, string $card): ResponseInterface
    {
        $payload = [
            'card' => $card
        ];
        return $this->sendRequest('POST', sprintf('/card-pay-test/%s', $paymentIntent), $payload);
    }

    public function confirmSplitPayment(string $splitPaymentId, ?string $paymentMethodId): ResponseInterface
    {
        $payload = [];
        if ($paymentMethodId) {
            $payload = [
                'payment_method' => $paymentMethodId
            ];
        }
        return $this->sendRequest('POST', sprintf('/api/split-payments/%s/confirm', $splitPaymentId), $payload);
    }

    public function getCollection($endpoint): ResponseInterface
    {
        if (!in_array($endpoint, self::VALID_RESOURCES)) {
            throw new \InvalidArgumentException('Invalid resource');
        }

        return $this->sendRequest('GET', sprintf('/api/%s', $endpoint));
    }

    public function getResource(string $endpoint, string $uuid): ResponseInterface
    {
        if (!in_array($endpoint, self::VALID_RESOURCES)) {
            throw new \InvalidArgumentException('Invalid resource');
        }

        if (!Uuid::isValid($uuid)) {
            throw new \InvalidArgumentException('Invalid uuid');
        }

        return $this->sendRequest('GET', sprintf('/api/%s/%s', $endpoint, $uuid));
    }

    public function putResource(string $endpoint, string $uuid, array $payload): ResponseInterface
    {
        if (!in_array($endpoint, self::VALID_RESOURCES)) {
            throw new \InvalidArgumentException('Invalid resource');
        }

        if (!Uuid::isValid($uuid)) {
            throw new \InvalidArgumentException('Invalid uuid');
        }

        return $this->sendRequest('PUT', sprintf('/api/%s/%s', $endpoint, $uuid), $payload);
    }

    public function postResource(string $endpoint, array $payload): ResponseInterface
    {
        if (!in_array($endpoint, self::VALID_RESOURCES)) {
            throw new \InvalidArgumentException('Invalid resource');
        }
        return $this->sendRequest('POST', sprintf('/api/%s', $endpoint), $payload);
    }

    public function sendRawRequest(string $method, string $endpoint, string $id = '', array $payload = [], string $template = '/api/{endpoint}'): ResponseInterface
    {
        if (!in_array($endpoint, self::VALID_RESOURCES)) {
            throw new \InvalidArgumentException('Invalid resource');
        }


        if (!strpos('endpoint', $template) === false) {
            throw new \InvalidArgumentException('`endpoint` must be on the template string');
        }

        $vars = array(
          '{endpoint}' => $endpoint,
          '{id}' => $id,
        );
        return $this->sendRequest('POST', strtr($template, $vars), $payload);
    }


    private function sendRequest(string $method, string $endpoint, array $payload = []): ResponseInterface
    {
        $uri = new Uri($this->uri);
        $uri = $uri->withPath(sprintf('%s', $endpoint));

        $request = new Request(
            $method,
            $uri,
            [
                'Accept' => 'application/ld+json',
                'Content-Type' => 'application/ld+json',
                'Authorization' => sprintf('Bearer %s', $this->getToken())
            ],
            json_encode($payload, JSON_THROW_ON_ERROR)
        );

        $response = $this->client->send($request, ['http_errors' => false]);

        $statusCode = $response->getStatusCode();
        if ($statusCode > 299) {
            $body = json_decode($response->getBody(), true);
            throw new ClientException(
                sprintf('Payment splitter %s status code', $statusCode),
                $request,
                $response,
                null,
                [isset($body['hydra:description']) ? $body['hydra:description'] : $body]
            );
        }

        return $response;
    }

    private function getToken(): string
    {
        $item = $this->cache->getItem('payment-splitter-token');
        if ($item->get()) {
            return $item->get();
        }
        $uri = new Uri($this->uri);
        $uri = $uri->withPath("login");
        $uri = $uri->withQuery("hash=$this->hash");

        $request = new Request(
            'POST',
            $uri,
            [],
            json_encode([
                'email' => $this->email
            ], JSON_THROW_ON_ERROR)
        );
        $response = $this->client->send($request);
        $payload = json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);

        if (!isset($payload['token'])) {
            throw new ClientException('Token not present', $request, $response);
        }

        $item->set($payload['token']);
        $item->expiresAfter(3600);
        $this->cache->save($item);

        return $payload['token'];
    }

    public function createWebhookEvent(string $content)
    {
        /**
         * @todo
         * credentials must be checked here
         */
        $data = \json_decode($content, true);
        return new Event($data);
    }
}
