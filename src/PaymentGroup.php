<?php

namespace Playfinder\PaymentSplitter;

use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;

class PaymentGroup extends ApiResource
{
    protected string $endpoint = 'payment-groups';

    protected int $amount;
    protected int $fee = 0;
    
    /**
     * @var ?array<string, null|int|string> $booker
     */
    protected ?array $booker = null;
    protected ?\DateTimeImmutable $cancelledAt = null;
    protected \DateTimeImmutable $createdAt;
    protected ?string $cancellationReason = null;
    protected string $captureMethod;
    protected string $currency;
    protected string $id;
    
    /**
     * @var ?array<string, mixed> $metadata
     */
    protected ?array $metadata;
    protected string $source;
    protected string $sourceObjectId;
    protected int $splitPaymentsNumber;
    /**
     * @var SplitPayment[] $splitPayments
     */
    protected array $splitPayments;
    protected string $state;
    protected \DateTimeImmutable $updatedAt;
    protected string $webHookUrl;

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    public function getFee(): int
    {
        return $this->fee;
    }

    public function setFee(int $fee): void
    {
        $this->fee = $fee;
    }

    public function getBooker(): ?array
    {
        return $this->booker;
    }

    public function setBooker(?array $booker): self
    {
        $this->booker = $booker;
        return $this;
    }

    public function getCancelledAt(): ?\DateTimeImmutable
    {
        return $this->cancelledAt;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getCancellationReason(): string
    {
        return $this->cancellationReason;
    }

    public function setCancellationReason(?string $cancellationReason): void
    {
        $this->cancellationReason = $cancellationReason;
    }

    public function getCaptureMethod(): string
    {
        return $this->captureMethod;
    }

    public function setCaptureMethod(string $captureMethod): self
    {
        $this->captureMethod = $captureMethod;
        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getMetadata(): array
    {
        return $this->metadata;
    }

    public function setMetadata(array $metadata): self
    {
        $this->metadata = $metadata;
        return $this;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;
        return $this;
    }

    public function getSourceObjectId(): string
    {
        return $this->sourceObjectId;
    }

    public function setSourceObjectId(string $sourceObjectId): self
    {
        $this->sourceObjectId = $sourceObjectId;
        return $this;
    }

    public function getAmountPaid(): int
    {
        return array_reduce(
            $this->getSplitPayments(),
            fn ($totalPaid, $splitPayment) => $totalPaid + (
                in_array($splitPayment->getState(), ['paid', 'captured']) ? $splitPayment->getAmount() : 0
            )
        );
    }

    public function getSplitPaymentsNumber(): int
    {
        return $this->splitPaymentsNumber;
    }

    public function getPaidSplitPaymentsNumber(): int
    {
        return array_reduce(
            $this->getSplitPayments(),
            fn ($paidPayments, $splitPayment) => $paidPayments + (
                in_array($splitPayment->getState(), ['paid', 'captured']) ? 1 : 0
            ),
            0
        );
    }


    public function getSplitPayments(): array
    {
        return $this->splitPayments;
    }

    public function setSplitPaymentsNumber(int $splitPaymentsNumber): self
    {
        $this->splitPaymentsNumber = $splitPaymentsNumber;
        return $this;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;
        return $this;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getWebHookUrl(): string
    {
        return $this->webHookUrl;
    }

    public function setWebHookUrl(string $webHookUrl): self
    {
        $this->webHookUrl = $webHookUrl;
        return $this;
    }
}
