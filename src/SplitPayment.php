<?php

namespace Playfinder\PaymentSplitter;

use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;

class SplitPayment extends ApiResource
{
    protected string $endpoint = 'split-payments';

    protected int $amount;
    /**
     * @var ?array<string, null|int|string> $attendee
     */
    protected ?array $attendee = null;
    protected \DateTimeImmutable $createdAt;
    protected string $id;
    protected bool $isBalanceSplitPayment;
    protected int $number;
    protected string $state;
    protected ?string $stripeId = null;
    protected \DateTimeImmutable $updatedAt;
    protected string $paymentGroupId;

    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return array<string, null|int|string>
     */
    public function getAttendee(): ?array
    {
        return $this->attendee;
    }
    /**
     * @param array<string, null|int|string> $attendee
     */
    public function setAttendee(?array $attendee): self
    {
        $this->attendee = $attendee;
        return $this;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getIsBalanceSplitPayment(): bool
    {
        return $this->isBalanceSplitPayment;
    }

    public function setIsBalanceSplitPayment(bool $isBalanceSplitPayment): self
    {
        $this->isBalanceSplitPayment = $isBalanceSplitPayment;
        return $this;
    }

    public function getNumber(): int
    {
        return $this->number;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;
        return $this;
    }

    public function getStripeId(): ?string
    {
        return $this->stripeId;
    }

    public function setStripeId(?string $stripeId): self
    {
        $this->stripeId = $stripeId;
        return $this;
    }

    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function getPaymentGroupId(): string
    {
        return $this->paymentGroupId;
    }

    /**
     * @param string $id URL for the request
     */
    public function confirm(string $id): self
    {
        $response = $this->client->sendRawRequest(
            'POST',
            $this->endpoint,
            $id,
            [],
            '/api/{endpoint}/{id}/confirm'
        );
        $this->setLastResponse($response);
        return $this;
    }
}
