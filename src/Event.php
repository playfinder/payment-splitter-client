<?php

namespace Playfinder\PaymentSplitter;

class Event
{
    const PAYMENT_TOTAL = 'payment.total';
    const PAYMENT_INDIVIDUAL = 'payment.individual';
    const PAYMENT_CREATE = 'payment.create';
    const PAYMENT_GROUP_CANCEL = 'payment.cancel.all';
    const PAYMENT_SPLIT_CANCEL = 'payment.cancel';
    const PAYMENT_CAPTURE = 'payment.capture';
    const PAYMENT_TRANSFER = 'payment.transfer';

    private string $id;
    private string $source;
    private string $type;
    private array $metadata;
    private array $object;

    public function getId(): string
    {
        return $this->id;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getMetadata(): array
    {
        return $this->metadata;
    }

    public function getObject(): array
    {
        return $this->object;
    }

    public function __construct(array $json)
    {
        $this->id = $json['sourceObjectId'];
        $this->source = $json['source'];
        $this->type = $json['type'];
        $this->metadata = $json['metadata'];
        $this->object = $json['object'];
    }
}
