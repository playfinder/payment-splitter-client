<?php

namespace Playfinder\PaymentSplitter;

use Psr\Http\Message\ResponseInterface;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;

abstract class ApiResource implements ApiResourceInterface
{
    protected Client $client;
    protected string $endpoint;
    protected ?ResponseInterface $lastResponse;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }
    /**
     * @param string $id URL for the request
     */
    public function get(string $id): self
    {
        $response = $this->client->getResource($this->endpoint, $id);
        $this->setLastResponse($response);
        return $this->hydrateFromResponse();
    }

    /**
     * @param array<string, mixed> $payload list of parameters for the request
     */
    public function post(array $payload): self
    {
        if (!$this->validPayload($payload)) {
            throw new \Exception('Sent POST payload contains not valid properties');
        }
        $response = $this->client->postResource($this->endpoint, $payload);
        $this->setLastResponse($response);
        return $this->hydrateFromResponse();
    }


    /**
     * @param string $id URL for the request
     * @param array<string, mixed> $payload list of parameters for the request
     */
    public function put(string $id, array $payload): self
    {
        if (!$this->validPayload($payload)) {
            throw new \Exception('Sent PUT payload contains not valid properties');
        }
        $response = $this->client->putResource($this->endpoint, $id, $payload);
        $this->setLastResponse($response);
        return $this->hydrateFromResponse();
    }

    protected function hydrateFromResponse(): self
    {
        if (!$this->lastResponse) {
            throw new \Exception('There is no response from the splitter server to hydrate');
        }
        return $this->hydrate(
            json_decode($this->lastResponse->getBody(), true, 512, JSON_THROW_ON_ERROR)
        );
    }

    public function hydrate(array $json): self
    {
        $class = get_class($this);
        $properties = array_keys(get_class_vars($class));

        $phpDocExtractor = new PhpDocExtractor();
        $reflectionExtractor = new ReflectionExtractor();
        $propertyInfo = new PropertyInfoExtractor(
            [$reflectionExtractor],
            [$phpDocExtractor, $reflectionExtractor]
        );

        foreach ($properties as $property) {
            if (isset($json[$property])) {
                $propertyAccessor = $propertyInfo->getTypes($class, $property)[0];
                $propertyBuiltinType = $propertyAccessor->getBuiltinType();
                $propertyClass = $propertyAccessor->getClassName();

                if (
                    $propertyBuiltinType === 'array' &&
                    count($propertyAccessor->getCollectionValueTypes()) > 0 &&
                    $propertyAccessor->getCollectionValueTypes()[0]->getClassName() &&
                    in_array(ApiResourceInterface::class, class_implements($propertyAccessor->getCollectionValueTypes()[0]->getClassName()) ?? [])
                ) {
                    $children = [];
                    foreach ($json[$property] as $child) {
                        $childClassName = $propertyAccessor->getCollectionValueTypes()[0]->getClassName();
                        $object = new $childClassName($this->client);
                        $children [] = $object->hydrate($child);

                    }
                    $this->{$property} = $children;
                    continue;
                }

                if ($propertyClass === 'DateTimeImmutable') {
                    $this->{$property} = new \DateTimeImmutable($json[$property]);
                    continue;
                }
                $this->{$property} = $json[$property];
            }
        }
        return $this;
    }

    public function getLastResponse(): ResponseInterface
    {
        return $this->lastResponse;
    }

    /**
     * Sets the last response from the Stripe API.
     *
     * @param ResponseInterface $response
     */
    public function setLastResponse(ResponseInterface $response)
    {
        $this->lastResponse = $response;
    }

    private function validPayload(array $payload): bool
    {
        return count(array_diff_key($payload, get_class_vars(get_class($this)))) === 0;
    }
}
