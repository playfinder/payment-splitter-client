<?php

namespace Playfinder\PaymentSplitter\Test;

use PHPUnit\Framework\MockObject\MockObject;
use Playfinder\PaymentSplitter\PaymentGroup;
use Playfinder\PaymentSplitter\SplitPayment;
use Playfinder\PaymentSplitter\Client;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Psr7\Response;

class PaymentGroupTest extends TestCase
{
    private PaymentGroup $paymentGroup;
    private $client;

    protected function setUp(): void
    {
        $this->client = $this->createMock(Client::class);
        $this->paymentGroup = new PaymentGroup($this->client);
    }

    public function testPost()
    {
        $this->client->expects($this->once())
            ->method('postResource')
            ->with('payment-groups', $this->anything())
            ->willReturn(new Response(200, [], '{"id":"eb832c20-52c1-4dea-a628-4743f9e29a97","amount":1000,"createdAt":"2022-02-06T18:48:40+00:00","updatedAt":"2022-02-06T18:48:40+00:00","createdBy":"elias@playfinder.com","cancelledAt":null,"cancellationReason":null,"captureMethod":"manual","currency":"GBP","metadata":[],"splitPaymentsNumber":10,"state":"open","source":"bookteq (mikelc)","sourceObjectId":"a2dedc2e-89db-4f26-ba71-6cdef37a8695","splitPayments":[{"id":"06d5aeb0-e01c-406b-8a8d-c46b0dd88279","amount":100,"state":"processing","stripeId":"pi_3KQFmuDhfhibuPdf0p0Yj8GR","isBalanceSplitPayment":false,"attendee":{"fullName":"Customer Foo","source":"bookteq (mikelc)","sourceObjectId":"Customer Foo"},"number":1},{"id":"1606a87b-149b-4f88-a9f3-4c0390cb37f7","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":2},{"id":"dad335a6-d2d0-4894-b4de-892eb884a8bd","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":3},{"id":"29b5bc17-9cba-4082-8773-ccb03c3953c8","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":4},{"id":"97b0ef60-e886-4897-b1da-544b79520251","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":5},{"id":"39a0224b-2627-4735-9e0c-a3be00c017d3","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":6},{"id":"0ff2a084-f95e-4912-804c-832d0d75bde6","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":7},{"id":"7f2ab2d9-b072-4778-b771-89d87dbf14ed","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":8},{"id":"c6465072-1979-41d6-996d-2358923e4371","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":9},{"id":"86cff822-facf-4f72-b9f1-6fd1e739a7a4","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":10}],"booker":{"fullName":"Customer Foo","source":"bookteq (mikelc)","sourceObjectId":"Customer Foo"},"webHookUrl":"http:\/\/api\/api\/splitter\/webhook"}'));

        $paymentGroup = $this->paymentGroup->post([
            "id" => "eb832c20-52c1-4dea-a628-4743f9e29a97",
            "amount" => 1000,
            "captureMethod" => "manual",
            "currency" => "GBP",
            "splitPaymentsNumber" => 10,
            "source" => "bookteq (mikelc)",
            "sourceObjectId" => "40697d55-9439-496e-ae44-8aa8f0c5af89",
            "booker" => [
                "fullName" => "Customer Foo",
                "source" => "bookteq (mikelc)",
                "sourceObjectId" => "Customer Foo"
            ],
            "webHookUrl" => "http:\/\/api\/api\/splitter\/webhook"
        ]);
        $this->assertSame("eb832c20-52c1-4dea-a628-4743f9e29a97", $paymentGroup->getId());
        $this->assertCount(10, $paymentGroup->getSplitPayments());
        $this->assertInstanceOf(SplitPayment::class, $paymentGroup->getSplitPayments()[0]);
        $this->assertSame(100, $paymentGroup->getSplitPayments()[0]->getAmount());
    }

    public function testGet()
    {
        $this->client->expects($this->once())
            ->method('getResource')
            ->with('payment-groups', $this->anything())
            ->willReturn(new Response(200, [], '{"id":"eb832c20-52c1-4dea-a628-4743f9e29a97","amount":1000,"createdAt":"2022-02-06T18:48:40+00:00","updatedAt":"2022-02-06T18:48:40+00:00","createdBy":"elias@playfinder.com","cancelledAt":null,"cancellationReason":null,"captureMethod":"manual","currency":"GBP","metadata":[],"splitPaymentsNumber":10,"state":"open","source":"bookteq (mikelc)","sourceObjectId":"a2dedc2e-89db-4f26-ba71-6cdef37a8695","splitPayments":[{"id":"06d5aeb0-e01c-406b-8a8d-c46b0dd88279","amount":100,"state":"processing","stripeId":"pi_3KQFmuDhfhibuPdf0p0Yj8GR","isBalanceSplitPayment":false,"attendee":{"fullName":"Customer Foo","source":"bookteq (mikelc)","sourceObjectId":"Customer Foo"},"number":1},{"id":"1606a87b-149b-4f88-a9f3-4c0390cb37f7","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":2},{"id":"dad335a6-d2d0-4894-b4de-892eb884a8bd","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":3},{"id":"29b5bc17-9cba-4082-8773-ccb03c3953c8","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":4},{"id":"97b0ef60-e886-4897-b1da-544b79520251","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":5},{"id":"39a0224b-2627-4735-9e0c-a3be00c017d3","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":6},{"id":"0ff2a084-f95e-4912-804c-832d0d75bde6","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":7},{"id":"7f2ab2d9-b072-4778-b771-89d87dbf14ed","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":8},{"id":"c6465072-1979-41d6-996d-2358923e4371","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":9},{"id":"86cff822-facf-4f72-b9f1-6fd1e739a7a4","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":10}],"booker":{"fullName":"Customer Foo","source":"bookteq (mikelc)","sourceObjectId":"Customer Foo"},"webHookUrl":"http:\/\/api\/api\/splitter\/webhook"}'));

        $paymentGroup = $this->paymentGroup->get('eb832c20-52c1-4dea-a628-4743f9e29a97');
        $this->assertSame("eb832c20-52c1-4dea-a628-4743f9e29a97", $paymentGroup->getId());
        $this->assertCount(10, $paymentGroup->getSplitPayments());
        $this->assertInstanceOf(SplitPayment::class, $paymentGroup->getSplitPayments()[0]);
        $this->assertSame(100, $paymentGroup->getSplitPayments()[0]->getAmount());
    }

    public function testPut()
    {
        $this->client->expects($this->once())
            ->method('putResource')
            ->with('payment-groups', $this->anything())
            ->willReturn(new Response(200, [], '{"id":"eb832c20-52c1-4dea-a628-4743f9e29a97","amount":1000,"createdAt":"2022-02-06T18:48:40+00:00","updatedAt":"2022-02-06T18:48:40+00:00","createdBy":"elias@playfinder.com","cancelledAt":null,"cancellationReason":null,"captureMethod":"manual","currency":"GBP","metadata":[],"splitPaymentsNumber":10,"state":"open","source":"bookteq (mikelc)","sourceObjectId":"a2dedc2e-89db-4f26-ba71-6cdef37a8695","splitPayments":[{"id":"06d5aeb0-e01c-406b-8a8d-c46b0dd88279","amount":100,"state":"processing","stripeId":"pi_3KQFmuDhfhibuPdf0p0Yj8GR","isBalanceSplitPayment":false,"attendee":{"fullName":"Customer Foo","source":"bookteq (mikelc)","sourceObjectId":"Customer Foo"},"number":1},{"id":"1606a87b-149b-4f88-a9f3-4c0390cb37f7","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":2},{"id":"dad335a6-d2d0-4894-b4de-892eb884a8bd","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":3},{"id":"29b5bc17-9cba-4082-8773-ccb03c3953c8","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":4},{"id":"97b0ef60-e886-4897-b1da-544b79520251","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":5},{"id":"39a0224b-2627-4735-9e0c-a3be00c017d3","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":6},{"id":"0ff2a084-f95e-4912-804c-832d0d75bde6","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":7},{"id":"7f2ab2d9-b072-4778-b771-89d87dbf14ed","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":8},{"id":"c6465072-1979-41d6-996d-2358923e4371","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":9},{"id":"86cff822-facf-4f72-b9f1-6fd1e739a7a4","amount":100,"state":"open","stripeId":null,"isBalanceSplitPayment":false,"attendee":null,"number":10}],"booker":{"fullName":"Customer Foo","source":"bookteq (mikelc)","sourceObjectId":"Customer Foo"},"webHookUrl":"http:\/\/api\/api\/splitter\/webhook"}'));

        $paymentGroup = $this->paymentGroup->put('eb832c20-52c1-4dea-a628-4743f9e29a97',[
            "id" => "eb832c20-52c1-4dea-a628-4743f9e29a97",
            "amount" => 1000,
            "captureMethod" => "manual",
            "currency" => "GBP",
            "splitPaymentsNumber" => 10,
            "source" => "bookteq (mikelc)",
            "sourceObjectId" => "40697d55-9439-496e-ae44-8aa8f0c5af89",
            "booker" => [
                "fullName" => "Customer Foo",
                "source" => "bookteq (mikelc)",
                "sourceObjectId" => "Customer Foo"
            ],
            "webHookUrl" => "http:\/\/api\/api\/splitter\/webhook"
        ]);
        $this->assertSame("eb832c20-52c1-4dea-a628-4743f9e29a97", $paymentGroup->getId());
        $this->assertCount(10, $paymentGroup->getSplitPayments());
        $this->assertInstanceOf(SplitPayment::class, $paymentGroup->getSplitPayments()[0]);
        $this->assertSame(100, $paymentGroup->getSplitPayments()[0]->getAmount());
    }

}
