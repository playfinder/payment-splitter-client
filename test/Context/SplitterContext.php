<?php

namespace Playfinder\PaymentSplitter\Test\Context;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Tester\Exception\PendingException;
use Behatch\Context\BaseContext;
use GuzzleHttp\Psr7\Request;
use Playfinder\PaymentSplitter\Client;

class SplitterContext extends BaseContext
{
    /** @var JsonContext */
    private $jsonContext;
    private string $jsonContextClass;
    private Client $client;

    public function __construct(Client $client, string $jsonContextClass)
    {
        $this->client = $client;
        $this->jsonContextClass = $jsonContextClass;
    }

    /** @BeforeScenario */
    public function gatherContexts(BeforeScenarioScope $scope): void
    {
        $environment = $scope->getEnvironment();
        $this->jsonContext = $environment->getContext($this->jsonContextClass);
    }

    /**
     * @Given I complete the Stripe transaction for :paymentIntentId on the Payment splitter
     * @Given I complete the Stripe transaction for :paymentIntentId on the Payment splitter with card :card
     */
    public function completeTheStripeTransactionOnSplitter(string $paymentIntentId, string $card = '4242424242424242')
    {
        $this->client->postPaymentIntent($this->jsonContext->parseTemplate($paymentIntentId), $card);
    }
}
